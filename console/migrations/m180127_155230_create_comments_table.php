<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m180127_155230_create_comments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'uid' => $this->integer(),
            'entity' => $this->string(),
            'title' => $this->string(),
            'description' => $this->string(),
            'date' => $this->timestamp(),
            'weight' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comments');
    }
}

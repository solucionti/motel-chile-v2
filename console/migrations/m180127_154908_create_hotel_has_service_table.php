<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel_has_service`.
 * Has foreign keys to the tables:
 *
 * - `hotel`
 * - `taxonomy`
 */
class m180127_154908_create_hotel_has_service_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('hotel_has_service', [
            'id' => $this->primaryKey(),
            'hotel_id' => $this->integer()->notNull(),
            'hotel_service_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `hotel_id`
        $this->createIndex(
            'idx-hotel_has_service-hotel_id',
            'hotel_has_service',
            'hotel_id'
        );

        // add foreign key for table `hotel`
        $this->addForeignKey(
            'fk-hotel_has_service-hotel_id',
            'hotel_has_service',
            'hotel_id',
            'hotel',
            'id',
            'CASCADE'
        );

        // creates index for column `hotel_service_id`
        $this->createIndex(
            'idx-hotel_has_service-hotel_service_id',
            'hotel_has_service',
            'hotel_service_id'
        );

        // add foreign key for table `taxonomy`
        $this->addForeignKey(
            'fk-hotel_has_service-hotel_service_id',
            'hotel_has_service',
            'hotel_service_id',
            'taxonomy',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `hotel`
        $this->dropForeignKey(
            'fk-hotel_has_service-hotel_id',
            'hotel_has_service'
        );

        // drops index for column `hotel_id`
        $this->dropIndex(
            'idx-hotel_has_service-hotel_id',
            'hotel_has_service'
        );

        // drops foreign key for table `taxonomy`
        $this->dropForeignKey(
            'fk-hotel_has_service-hotel_service_id',
            'hotel_has_service'
        );

        // drops index for column `hotel_service_id`
        $this->dropIndex(
            'idx-hotel_has_service-hotel_service_id',
            'hotel_has_service'
        );

        $this->dropTable('hotel_has_service');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `media`.
 */
class m180127_154029_create_media_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('media', [
            'id' => $this->primaryKey(),
            'uid' => $this->integer(),
            'entity' => $this->string(),
            'title' => $this->string(),
            'description' => $this->string(),
            'uri' => $this->string(),
            'type' => $this->string(),
            'size' => $this->string(),
            'weight' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('media');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel_room`.
 * Has foreign keys to the tables:
 *
 * - `hotel`
 */
class m180127_154320_create_hotel_room_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('hotel_room', [
            'id' => $this->primaryKey(),
            'hotel_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'description' => $this->text(),
        ]);

        // creates index for column `hotel_id`
        $this->createIndex(
            'idx-hotel_room-hotel_id',
            'hotel_room',
            'hotel_id'
        );

        // add foreign key for table `hotel`
        $this->addForeignKey(
            'fk-hotel_room-hotel_id',
            'hotel_room',
            'hotel_id',
            'hotel',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `hotel`
        $this->dropForeignKey(
            'fk-hotel_room-hotel_id',
            'hotel_room'
        );

        // drops index for column `hotel_id`
        $this->dropIndex(
            'idx-hotel_room-hotel_id',
            'hotel_room'
        );

        $this->dropTable('hotel_room');
    }
}

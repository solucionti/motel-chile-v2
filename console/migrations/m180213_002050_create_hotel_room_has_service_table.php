<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel_room_has_service`.
 * Has foreign keys to the tables:
 *
 * - `hotel_room`
 * - `taxonomy`
 */
class m180213_002050_create_hotel_room_has_service_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('hotel_room_has_service', [
            'id' => $this->primaryKey(),
            'hotel_room_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `hotel_room_id`
        $this->createIndex(
            'idx-hotel_room_has_service-hotel_room_id',
            'hotel_room_has_service',
            'hotel_room_id'
        );

        // add foreign key for table `hotel_room`
        $this->addForeignKey(
            'fk-hotel_room_has_service-hotel_room_id',
            'hotel_room_has_service',
            'hotel_room_id',
            'hotel_room',
            'id',
            'CASCADE'
        );

        // creates index for column `service_id`
        $this->createIndex(
            'idx-hotel_room_has_service-service_id',
            'hotel_room_has_service',
            'service_id'
        );

        // add foreign key for table `taxonomy`
        $this->addForeignKey(
            'fk-hotel_room_has_service-service_id',
            'hotel_room_has_service',
            'service_id',
            'taxonomy',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `hotel_room`
        $this->dropForeignKey(
            'fk-hotel_room_has_service-hotel_room_id',
            'hotel_room_has_service'
        );

        // drops index for column `hotel_room_id`
        $this->dropIndex(
            'idx-hotel_room_has_service-hotel_room_id',
            'hotel_room_has_service'
        );

        // drops foreign key for table `taxonomy`
        $this->dropForeignKey(
            'fk-hotel_room_has_service-service_id',
            'hotel_room_has_service'
        );

        // drops index for column `service_id`
        $this->dropIndex(
            'idx-hotel_room_has_service-service_id',
            'hotel_room_has_service'
        );

        $this->dropTable('hotel_room_has_service');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel_room_schedule`.
 * Has foreign keys to the tables:
 *
 * - `hotel_room`
 */
class m180211_215835_create_hotel_room_schedule_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('hotel_room_schedule', [
            'id'              => $this->primaryKey(),
            'hotel_room_id'   => $this->integer()->notNull(),
            'week_day_init'   => $this->integer(),
            'week_day_finish' => $this->integer(),
            'week_day_finish' => $this->integer(),
            'hours'           => $this->integer(),
            'price'           => $this->integer(),
        ]);

        // creates index for column `hotel_room_id`
        $this->createIndex(
            'idx-hotel_room_schedule-hotel_room_id',
            'hotel_room_schedule',
            'hotel_room_id'
        );

        // add foreign key for table `hotel_room`
        $this->addForeignKey(
            'fk-hotel_room_schedule-hotel_room_id',
            'hotel_room_schedule',
            'hotel_room_id',
            'hotel_room',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `hotel_room`
        $this->dropForeignKey(
            'fk-hotel_room_schedule-hotel_room_id',
            'hotel_room_schedule'
        );

        // drops index for column `hotel_room_id`
        $this->dropIndex(
            'idx-hotel_room_schedule-hotel_room_id',
            'hotel_room_schedule'
        );

        $this->dropTable('hotel_room_schedule');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `vocabulary`.
 */
class m180126_235656_create_vocabulary_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('vocabulary', [
            'id'           => $this->primaryKey(),
            'title'        => $this->string()->notNull()->unique(),
            'machine_name' => $this->string()->notNull()->unique(),
            'description'  => $this->text(),
        ]);

        $this->insert('vocabulary', [
            'id'           => 1,
            'title'        => 'Servicios',
            'machine_name' => 'servicios',
            'description'  => 'Lista los servicios del hotel'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('vocabulary');
    }
}

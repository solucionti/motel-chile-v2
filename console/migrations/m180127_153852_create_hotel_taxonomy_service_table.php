<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel_taxonomy_service`.
 * Has foreign keys to the tables:
 *
 * - `taxonomy`
 */
class m180127_153852_create_hotel_taxonomy_service_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('hotel_taxonomy_service', [
            'id' => $this->primaryKey(),
            'taxonomy_id' => $this->integer()->notNull(),
            'logo' => $this->string(),
        ]);

        // creates index for column `taxonomy_id`
        $this->createIndex(
            'idx-hotel_taxonomy_service-taxonomy_id',
            'hotel_taxonomy_service',
            'taxonomy_id'
        );

        // add foreign key for table `taxonomy`
        $this->addForeignKey(
            'fk-hotel_taxonomy_service-taxonomy_id',
            'hotel_taxonomy_service',
            'taxonomy_id',
            'taxonomy',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `taxonomy`
        $this->dropForeignKey(
            'fk-hotel_taxonomy_service-taxonomy_id',
            'hotel_taxonomy_service'
        );

        // drops index for column `taxonomy_id`
        $this->dropIndex(
            'idx-hotel_taxonomy_service-taxonomy_id',
            'hotel_taxonomy_service'
        );

        $this->dropTable('hotel_taxonomy_service');
    }
}

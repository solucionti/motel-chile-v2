<?php

use yii\db\Migration;

/**
 * Handles the creation of table `taxonomy`.
 * Has foreign keys to the tables:
 *
 * - `vocabulary`
 * - `taxonomy`
 */
class m180127_000107_create_taxonomy_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('taxonomy', [
            'id'            => $this->primaryKey(),
            'vocabulary_id' => $this->integer()->notNull(),
            'parent'        => $this->integer(),
            'title'         => $this->string()->notNull()->unique(),
            'machine_name'  => $this->string()->notNull()->unique(),
            'description'   => $this->text(),
        ]);

        // creates index for column `vocabulary_id`
        $this->createIndex(
            'idx-taxonomy-vocabulary_id',
            'taxonomy',
            'vocabulary_id'
        );

        // add foreign key for table `vocabulary`
        $this->addForeignKey(
            'fk-taxonomy-vocabulary_id',
            'taxonomy',
            'vocabulary_id',
            'vocabulary',
            'id',
            'CASCADE'
        );

        // creates index for column `parent`
        $this->createIndex(
            'idx-taxonomy-parent',
            'taxonomy',
            'parent'
        );

        // add foreign key for table `taxonomy`
        $this->addForeignKey(
            'fk-taxonomy-parent',
            'taxonomy',
            'parent',
            'taxonomy',
            'id',
            'CASCADE'
        );

        $this->insert('taxonomy', [
            'vocabulary_id' => 1,
            'title'         => 'Ambientes',
            'machine_name'  => 'ambientes',
        ]);
        $this->insert('taxonomy', [
            'vocabulary_id' => 1,
            'title'         => 'Baño Privado',
            'machine_name'  => 'baño_privado',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `vocabulary`
        $this->dropForeignKey(
            'fk-taxonomy-vocabulary_id',
            'taxonomy'
        );

        // drops index for column `vocabulary_id`
        $this->dropIndex(
            'idx-taxonomy-vocabulary_id',
            'taxonomy'
        );

        // drops foreign key for table `taxonomy`
        $this->dropForeignKey(
            'fk-taxonomy-parent',
            'taxonomy'
        );

        // drops index for column `parent`
        $this->dropIndex(
            'idx-taxonomy-parent',
            'taxonomy'
        );

        $this->dropTable('taxonomy');
    }
}

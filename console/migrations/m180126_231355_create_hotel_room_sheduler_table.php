<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel_room_sheduler`.
 * Has foreign keys to the tables:
 *
 * - `hotel_room`
 */
class m180126_231355_create_hotel_room_sheduler_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        //./yii migrate/create create_hotel_room_sheduler_table --fields="hotel_room_id:integer:notNull:foreignKey(hotel_room),price:integer,date_ini:date,date_end:date"

        //$this->createTable('hotel_room_sheduler', [
        //    'id' => $this->primaryKey(),
        //    'hotel_room_id' => $this->integer()->notNull(),
        //    'price' => $this->integer(),
        //    'time_ini' => $this->time(),
        //    'time_end' => $this->time(),
        //]);
        //
        //// creates index for column `hotel_room_id`
        //$this->createIndex(
        //    'idx-hotel_room_sheduler-hotel_room_id',
        //    'hotel_room_sheduler',
        //    'hotel_room_id'
        //);
        //
        //// add foreign key for table `hotel_room`
        //$this->addForeignKey(
        //    'fk-hotel_room_sheduler-hotel_room_id',
        //    'hotel_room_sheduler',
        //    'hotel_room_id',
        //    'hotel_room',
        //    'id',
        //    'CASCADE'
        //);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        //    // drops foreign key for table `hotel_room`
        //    $this->dropForeignKey(
        //        'fk-hotel_room_sheduler-hotel_room_id',
        //        'hotel_room_sheduler'
        //    );
        //
        //    // drops index for column `hotel_room_id`
        //    $this->dropIndex(
        //        'idx-hotel_room_sheduler-hotel_room_id',
        //        'hotel_room_sheduler'
        //    );
        //
        //    $this->dropTable('hotel_room_sheduler');
    }
}

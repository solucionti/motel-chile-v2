<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel`.
 */
class m180126_223747_create_hotel_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('hotel', [
            'id'       => $this->primaryKey(),
            'title'    => $this->string(),
            'body'     => $this->text(),
            'logo'     => $this->string(),
            'email'    => $this->string(),
            'website'  => $this->string(),
            'location' => $this->integer(),
            'slug'     => $this->integer(),
        ]);


        //./yii migrate/create create_hotel_has_service_table --fields="hotel_id:integer:notNull:foreignKey(hotel),hotel_service_id:integer:notNull:foreignKey(hotel_service)"

        //$this->createTable('hotel_service', [
        //    'id'    => $this->primaryKey(),
        //    'title' => $this->string(),
        //    'logo'  => $this->string(),
        //]);
        //
        //$this->createTable('hotel_has_service', [
        //    'id'               => $this->primaryKey(),
        //    'hotel_id'         => $this->integer()->notNull(),
        //    'hotel_service_id' => $this->integer(),
        //]);
        //
        //$this->addForeignKey(
        //    'fk-hotel-service',
        //    'hotel_has_service',
        //    'hotel_id',
        //    'hotel',
        //    'id',
        //    ''
        //);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('hotel');
    }
}

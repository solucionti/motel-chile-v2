<?php

use app\models\hotel\HotelRoomSchedule;
use demogorgorn\ajax\AjaxSubmitButton;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\components\helpers\HelperClass;
use wbraganca\dynamicform\DynamicFormWidget;


$hotelRoomSchedule = (empty($room->hotelRoomSchedules)) ? [new HotelRoomSchedule()] : $room->hotelRoomSchedules


?>

<span class="m-section__sub">
<button type="button" class="btn btn-success" data-toggle="modal"
        data-target="#horarios_modal_<?php echo $room->id ?>">
Horarios</button>
</span>

<div class="modal fade" id="horarios_modal_<?php echo $room->id ?>" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Horarios
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <?php $form_schedule = ActiveForm::begin([
                    // 'action'                 => ['hotelroomschedule/create', 'id' => $room->id],
                    'enableClientValidation' => true,
                    // 'enableAjaxValidation'   => true,
                    'validateOnChange'       => true,
                    // 'validateOnBlur'         => true,
                    'options'                => [
                        'enctype' => 'multipart/form-data',
                        'id'      => 'formulario_horarios_' . $room->id
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody'      => '.container-items', // required: css class selector
                        'widgetItem'      => '.item', // required: css class
                        'limit'           => 4, // the maximum times, an element can be cloned (default 999)
                        'min'             => 1, // 0 or 1 (default 1)
                        'insertButton'    => '.add-item', // css class
                        'deleteButton'    => '.remove-item', // css class
                        'model'           => $hotelRoomSchedule[0],
                        'formId'          => 'formulario_horarios_' . $room->id,
                        'formFields'      => [
                            'week_day_init',
                            'week_day_finish',
                            'hours',
                            'price',
                        ],
                    ]); ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-envelope"></i> Precios Habitación <?php echo $room->title ?>
                            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i
                                        class="fa fa-plus"></i> Agregar nuevo Horario
                            </button>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body container-items"><!-- widgetContainer -->
                            <?= Html::activeHiddenInput($room, 'id'); ?>
                            <?php foreach ($hotelRoomSchedule as $index => $modelScheduler): ?>
                                <div class="item panel panel-default"><!-- widgetBody -->
                                    <div class="panel-heading">

                                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i
                                                    class="fa fa-minus"></i></button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">

                                        <?php


                                        if (!$modelScheduler->isNewRecord) {

                                            echo Html::activeHiddenInput($modelScheduler, "[{$index}]id");
                                        }
                                        ?>

                                        <div class="row">
                                            <div class="col-md-3">

                                                <?= $form_schedule->field($modelScheduler, "[{$index}]week_day_init")->dropDownList(HelperClass::getDaysOfWeek()) ?>
                                            </div>
                                            <div class="col-md-3">
                                                <?= $form_schedule->field($modelScheduler, "[{$index}]week_day_finish")->dropDownList(HelperClass::getDaysOfWeek()) ?>
                                            </div>
                                            <div class="col-md-3">
                                                <?= $form_schedule->field($modelScheduler, "[{$index}]hours")->dropDownList(HelperClass::getHoursOfDay()) ?>

                                            </div>
                                            <div class="col-md-3">
                                                <?= $form_schedule->field($modelScheduler, "[{$index}]price")->textInput(['maxlength' => true]) ?>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <?php DynamicFormWidget::end(); ?>

                </div>

                <div class="form-group">

                    <?php
                    AjaxSubmitButton::begin([
                        'label'             => 'Asignar horarios',
                        'useWithActiveForm' => 'formulario_horarios_' . $room->id,

                        'ajaxOptions' => [
                            'type'    => 'POST',
                            'url'     => Url::to(['hotelroomschedule/create', 'id' => $room->id]),
                            'success' => new \yii\web\JsExpression("function(result) {
                            alert('Datos actualizados');
                                                        
            }"),
                        ],
                        'options'     => ['class' => 'btn btn-success', 'type' => 'submit', 'id' => 'add_horario_' . $room->id],
                    ]);
                    AjaxSubmitButton::end();

                    ?>

                </div>
            </div>


            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<?php
use dosamigos\fileupload\FileUploadUI;

?>

<?php echo FileUploadUI::widget([
    'model'         => $media,
    'attribute'     => 'image',
    'url'           => ['media/imageupload', ['id' => $model->id, 'type' => 'HOTEL_ROOM']],
    'gallery'       => true,
    'fieldOptions'  => [
        'accept' => 'image/*'
    ],
    'clientOptions' => [
        'maxFileSize' => 2000000
    ],
    'clientEvents'  => [
        'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
    ],
]); ?>



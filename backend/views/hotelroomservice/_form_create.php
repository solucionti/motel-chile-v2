<?php

use app\models\hotel\HotelRoomHasService;
use common\components\taxonomy\TaxonomyClassHelper;
use demogorgorn\ajax\AjaxSubmitButton;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


$hotelRoomServices = new HotelRoomHasService();

$hotelRoomServices->services = $room->getServicesAsArray();




$services = TaxonomyClassHelper::listServices();

?>

<span class="m-section__sub">
<button type="button" class="btn btn-success" data-toggle="modal"
        data-target="#servicios_modal_<?php echo $room->id ?>">
Ver Servicios</button>
</span>

<div class="modal fade" id="servicios_modal_<?php echo $room->id ?>" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Servicios
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <?php $form = ActiveForm::begin([
                    // 'action'                 => ['hotelroomschedule/create', 'id' => $room->id],
                    'enableClientValidation' => true,
                    // 'enableAjaxValidation'   => true,
                    'validateOnChange'       => true,
                    // 'validateOnBlur'         => true,
                    'options'                => [
                        //      'enctype' => 'multipart/form-data',
                        'id' => 'formulario_servicios_' . $room->id
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?= $form->field($hotelRoomServices, 'services')->checkboxList($services) ?>

                </div>

                <div class="form-group">

                    <?php
                    AjaxSubmitButton::begin([
                        'label'             => 'Asignar Servicios',
                        'useWithActiveForm' => 'formulario_servicios_' . $room->id,

                        'ajaxOptions' => [
                            'type'    => 'POST',
                            'url'     => Url::to(['hotelroomservice/roomservice', 'id' => $room->id]),
                            'success' => new \yii\web\JsExpression("function(result) {
                            alert('Datos actualizados');
                                                        
            }"),
                        ],
                        'options'     => ['class' => 'btn btn-success', 'type' => 'submit', 'id' => 'add_servicio_' . $room->id],
                    ]);
                    AjaxSubmitButton::end();

                    ?>

                </div>
            </div>


            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
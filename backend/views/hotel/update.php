<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\hotel\Hotel */

$this->title = 'Update Hotel: '. $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Hotels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="m-content">
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                            <h3 class="m-portlet__head-text"><?php echo $this->title ?> </h3>
                        </div>
                    </div>
                </div>


                <?= $this->render('_form', [
                    'model'      => $model,
                    'model_room' => $model_room,
                    'media'      => $media,



                ]) ?>

            </div>
        </div>
    </div>
</div>
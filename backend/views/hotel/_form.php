<?php

use common\components\taxonomy\TaxonomyClassHelper;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\jui\JuiAsset;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\FileInput;
use yii\helpers\Url;
use app\models\media\Media;


$services = TaxonomyClassHelper::listServices();
/* @var $this yii\web\View */
/* @var $model app\models\hotel\Hotel */
/* @var $form yii\widgets\ActiveForm */


?>



<?php $form = ActiveForm::begin(['options' => [
    'enctype' => 'multipart/form-data',
    'id'      => 'dynamic-form'
]]); ?>
<div class="m-portlet__body">

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location')->textInput() ?>

    <?= $form->field($model, 'slug')->textInput() ?>

    <?= $form->field($model, 'services')->checkboxList($services) ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php if (!$model->isNewRecord): ?>
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                  <span class="m-portlet__head-icon m--hide">
                  <i class="la la-gear"></i>
                  </span>
                            <h3 class="m-portlet__head-text">
                                Habitaciones
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <?php
                        echo $this->render('partials/_new_room', [
                            'model'      => $model,
                            'model_room' => $model_room,
                            'media'      => $media,
                        ]);
                        ?>
                        <div id="room_table">
                            <?php
                            echo $this->render('partials/_rooms_table', [
                                'model' => $model,
                            ]);
                            ?>
                        </div>
                    </div>
                    <!--end::Section-->
                    <!--begin::Section-->
                    <!--end::Section-->
                </div>
            </div>
            <!--end::Portlet-->
            <!--begin::Portlet-->
            <!--end::Portlet-->
            <!--begin::Portlet-->
            <!--end::Portlet-->
        </div>
    </div>

<?php endif; ?>




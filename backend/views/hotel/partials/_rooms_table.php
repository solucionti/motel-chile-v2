<?php use app\models\hotel\HotelRoomSchedule;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

if ($model->hotelRooms): ?>

    <div class="m-section__content">
        <table class="table">
            <thead>
            <tr>

                <th>Título</th>
                <th>Galeria de Imagenes</th>
                <th>Horarios</th>
                <th>Servicios</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($model->hotelRooms as $room): ?>
                <tr>
                    <td><?php echo $room->title ?></td>
                    <td>
                        <?php
                        echo $this->render('_gallery_form', [
                            'room' => $room,

                        ]);
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $this->render('//hotelroomschedule/_form_create', [
                                'room' => $room,


                            ]);
                            ?>

                    </td>
                    <td>
                        <?php
                        echo $this->render('//hotelroomservice/_form_create', [
                            'room' => $room,



                        ]);
                        ?>
                    </td>
                    <td>
                        <button type="button" class="btn btn-info">
                            Ver Detalle
                        </button>
                        <a href="/hotel/hotelroomdelete/<?php echo $room->id?>" class="btn btn-danger" data-pjax="0"
                           data-confirm="¿ Estás seguro que deseas borrar el item <?php echo $room->title?> ?"
                           data-method="post">Eliminar</a>

                    </td>
                </tr>
            <?php endforeach; ?>


            </tbody>
        </table>
    </div>
<?php endif; ?>


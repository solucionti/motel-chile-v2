<?php
use demogorgorn\ajax\AjaxSubmitButton;
use dosamigos\fileupload\FileUploadUI;
use yii\widgets\ActiveForm;
use yii\helpers\Html;


?>

<span class="m-section__sub">
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#nueva_habitacion">
Agregar nueva habitación
</button>
               </span>

<div class="modal fade" id="nueva_habitacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Nueva Habitación
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                </button>
            </div>
            <div class="modal-body">


                <?php $form_add_room = ActiveForm::begin(['options' => [
                    'enctype' => 'multipart/form-data',
                    'id'      => 'add-room'
                ]]); ?>


                <?= $form_add_room->field($model_room, 'hotel_id')->hiddenInput(['value' => $model->id])->label(false); ?>

                <?= $form_add_room->field($model_room, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form_add_room->field($model_room, 'description')->textarea() ?>


            </div>
            <div class="modal-footer">

                <div class="form-group">
                    <?php
                    AjaxSubmitButton::begin([
                        'label'             => 'Crear Nueva Habitación',
                        'useWithActiveForm' => 'add-room',

                        'ajaxOptions' => [
                            'type'    => 'POST',
                            'url'     => '/hotel/createroom',
                            'success' => new \yii\web\JsExpression("function(result) {
                            console.log(result);
                            $('#room_table').html(result.data.refresh);
                                                        
            }"),
                        ],
                        'options'     => ['class' => 'btn btn-success', 'type' => 'submit', 'id' => 'add-button'],
                    ]);
                    AjaxSubmitButton::end();

                    ?>
                </div>
            </div>


            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
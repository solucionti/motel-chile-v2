<?php
use app\models\media\Media;
use dosamigos\fileupload\FileUploadUI;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;


?>

<span class="m-section__sub">
<button type="button" class="btn btn-success" data-toggle="modal"
        data-target="#galeria_imagenes_<?php echo $room->id ?>">
Galería de Imagenes
</button>
</span>

<div class="modal fade" id="galeria_imagenes_<?php echo $room->id ?>" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Galería Imagenes
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <?php foreach ($room->getGallery() as $image): ?>
                        <div class="col-md-2">
                            <?php echo Html::img(Yii::$app->params['URL_FRONT_PUBLIC_FILES_THUMBS'] . '100x100_' . $image->uri) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php $form_gallery = ActiveForm::begin(['options' => [
                    'enctype' => 'multipart/form-data',
                    'id'      => 'galeria_imagenes_' . $room->id
                ]]);
                ?>
                <div class="form-group">
                    <?php
                    echo FileUploadUI::widget([
                        'model'         => new Media(),
                        'attribute'     => 'image['. $room->id.']',
                        'url'           => Url::to(['media/imageupload/', 'type' => 'HOTEL_ROOM', 'id' => $room->id]),
                        'gallery'       => false,
                        'fieldOptions'  => [
                            'accept' => 'image/*'
                        ],
                        'clientOptions' => [
                            'maxFileSize' => 2000000
                        ],
                        'clientEvents'  => [
                            'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                        ],
                    ]); ?>
                </div>
            </div>


            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
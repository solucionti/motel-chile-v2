<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log'],
    'modules'             => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],

    ],
    'components'          => [
        //'view'         => [
        //    'theme' => [
        //        //'pathMap'  => ['@app/views' => '@web/themes/admin/views'],
        //        //'baseUrl'  => '@web/../themes/admin',
        //        'basePath' => '@frontend/themes/admin',
        //        'baseUrl'  => '@web/themes/admin',
        //        'pathMap'  => [
        //            '@backend/views' => '@backend/web/themes/admin/views',
        //        ],
        //    ],
        //],
        'assetManager' => [
            'bundles' => [
                //'yii\web\JqueryAsset' => [
                //    'jsOptions' => ['position' => \yii\web\View::POS_HEAD],
                //],
                //'wbraganca\dynamicform\DynamicFormAsset' => [
                //    'sourcePath' => '@app/web/js',
                //    'js' => [
                //        'yii2-dynamic-form.js'
                //    ],
                //],
            ],
        ],

        'request'      => [
            'csrfParam' => '_csrf-backend',
        ],
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session'      => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'class'           => 'yii\web\UrlManager',
            // Hide index.php
            'showScriptName'  => false,
            // Use pretty URLs
            'enablePrettyUrl' => true,
            'rules'           => [
                'imageupload/<type:\w+>/<id:\d+>'  => 'media/imageupload',
                '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
            ],
        ],
    ],
    'params'              => $params,
];

<?php

namespace app\models\hotel;

use Yii;

use app\models\vocabulary\Taxonomy;

/**
 * This is the model class for table "hotel_has_service".
 *
 * @property int $id
 * @property int $hotel_id
 * @property int $hotel_service_id
 *
 * @property Hotel $hotel
 * @property Taxonomy $hotelService
 */
class HotelHasService extends \yii\db\ActiveRecord
{

    public $services = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_has_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id', 'hotel_service_id', 'services'], 'required'],
            [['hotel_id', 'hotel_service_id'], 'integer'],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['hotel_id' => 'id']],
            [['hotel_service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Taxonomy::className(), 'targetAttribute' => ['hotel_service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('app', 'ID'),
            'hotel_id'         => Yii::t('app', 'Hotel ID'),
            'hotel_service_id' => Yii::t('app', 'Hotel Service ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id'])->inverseOf('hotelHasServices');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelService()
    {
        return $this->hasOne(Taxonomy::className(), ['id' => 'hotel_service_id'])->inverseOf('hotelHasServices');
    }


}

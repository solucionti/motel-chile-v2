<?php

namespace app\models\hotel;

use app\models\vocabulary\Taxonomy;
use Yii;

/**
 * This is the model class for table "hotel_room_has_service".
 *
 * @property int $id
 * @property int $hotel_room_id
 * @property int $service_id
 *
 * @property HotelRoom $hotelRoom
 * @property Taxonomy $service
 */
class HotelRoomHasService extends \yii\db\ActiveRecord
{
    public $services = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_room_has_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_room_id', 'service_id'], 'required'],
            [['hotel_room_id', 'service_id'], 'integer'],
            [['hotel_room_id'], 'exist', 'skipOnError' => true, 'targetClass' => HotelRoom::className(), 'targetAttribute' => ['hotel_room_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Taxonomy::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'hotel_room_id' => Yii::t('app', 'Hotel Room ID'),
            'service_id' => Yii::t('app', 'Service ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRoom()
    {
        return $this->hasOne(HotelRoom::className(), ['id' => 'hotel_room_id'])->inverseOf('hotelRoomHasServices');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Taxonomy::className(), ['id' => 'service_id'])->inverseOf('hotelRoomHasServices');
    }
}

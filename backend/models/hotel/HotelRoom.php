<?php

namespace app\models\hotel;

use app\models\media\Media;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "hotel_room".
 *
 * @property int $id
 * @property int $hotel_id
 * @property string $title
 * @property string $description
 *
 * @property Hotel $hotel
 * @property HotelRoomHasService[] $hotelRoomHasServices
 * @property HotelRoomSchedule[] $hotelRoomSchedules
 */
class HotelRoom extends \yii\db\ActiveRecord
{

    public $gallery;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'maxSize' => 20 * 1024 * 1024, 'maxFiles' => 0],

            [['hotel_id', 'title', 'description'], 'required'],
            [['hotel_id'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['hotel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'hotel_id'    => Yii::t('app', 'Hotel ID'),
            'title'       => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id'])->inverseOf('hotelRooms');
    }

    public function getGallery()
    {
        return Media::find()->where(['uid' => $this->id])->andWhere(['entity' => 'HOTEL_ROOM'])->all();

        return;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRoomHasServices()
    {
        return $this->hasMany(HotelRoomHasService::className(), ['hotel_room_id' => 'id'])->inverseOf('hotelRoom');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRoomSchedules()
    {
        return $this->hasMany(HotelRoomSchedule::className(), ['hotel_room_id' => 'id'])->inverseOf('hotelRoom');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function deleteAllService()
    {
        HotelRoomHasService::deleteAll(['hotel_room_id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesAsArray()
    {
        return ArrayHelper::map(HotelRoomHasService::find()
            ->asArray()
            ->select('service_id')
            ->where(['hotel_room_id' => $this->id])
            ->all(), 'service_id', 'service_id');

    }
}

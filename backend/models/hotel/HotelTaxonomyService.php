<?php

namespace app\models\hotel;

use Yii;

/**
 * This is the model class for table "hotel_taxonomy_service".
 *
 * @property int $id
 * @property int $taxonomy_id
 * @property string $logo
 *
 * @property Taxonomy $taxonomy
 */
class HotelTaxonomyService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_taxonomy_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taxonomy_id'], 'required'],
            [['taxonomy_id'], 'integer'],
            [['logo'], 'string', 'max' => 255],
            [['taxonomy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Taxonomy::className(), 'targetAttribute' => ['taxonomy_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'taxonomy_id' => Yii::t('app', 'Taxonomy ID'),
            'logo'        => Yii::t('app', 'Logo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxonomy()
    {
        return $this->hasOne(Taxonomy::className(), ['id' => 'taxonomy_id'])->inverseOf('hotelTaxonomyServices');
    }
}

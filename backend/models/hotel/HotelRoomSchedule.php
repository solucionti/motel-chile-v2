<?php

namespace app\models\hotel;

use Yii;

/**
 * This is the model class for table "hotel_room_schedule".
 *
 * @property int $id
 * @property int $hotel_room_id
 * @property int $week_day_init
 * @property int $week_day_finish
 * @property int $hours
 * @property int $price
 *
 * @property HotelRoom $hotelRoom
 */
class HotelRoomSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_room_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_room_id','price'], 'required'],
            [['hotel_room_id', 'week_day_init', 'week_day_finish', 'hours', 'price'], 'integer'],
            [['hotel_room_id'], 'exist', 'skipOnError' => true, 'targetClass' => HotelRoom::className(), 'targetAttribute' => ['hotel_room_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('app', 'ID'),
            'hotel_room_id'   => Yii::t('app', 'Hotel Room ID'),
            'week_day_init'   => Yii::t('app', 'Week Day Init'),
            'week_day_finish' => Yii::t('app', 'Week Day Finish'),
            'hours'           => Yii::t('app', 'Hours'),
            'price'           => Yii::t('app', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRoom()
    {
        return $this->hasOne(HotelRoom::className(), ['id' => 'hotel_room_id'])->inverseOf('hotelRoomSchedules');
    }
}

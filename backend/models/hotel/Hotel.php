<?php

namespace app\models\hotel;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "hotel".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property string $logo
 * @property string $email
 * @property string $website
 * @property int $location
 * @property int $slug
 *
 * @property HotelHasService[] $hotelHasServices
 * @property HotelRoom[] $hotelRooms
 */
class Hotel extends \yii\db\ActiveRecord
{

    public $services = [];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'email', 'services'], 'required'],
            [['body'], 'string'],
            [['location', 'slug'], 'integer'],
            [['title', 'logo', 'email', 'website'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => Yii::t('app', 'ID'),
            'title'    => Yii::t('app', 'Title'),
            'body'     => Yii::t('app', 'Body'),
            'logo'     => Yii::t('app', 'Logo'),
            'email'    => Yii::t('app', 'Email'),
            'website'  => Yii::t('app', 'Website'),
            'location' => Yii::t('app', 'Location'),
            'slug'     => Yii::t('app', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelHasServices()
    {
        return $this->hasMany(HotelHasService::className(), ['hotel_id' => 'id'])->inverseOf('hotel');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRooms()
    {
        return $this->hasMany(HotelRoom::className(), ['hotel_id' => 'id'])->inverseOf('hotel');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesAsArray()
    {
        return ArrayHelper::map(HotelHasService::find()
            ->asArray()
            ->select('hotel_service_id')
            ->where(['hotel_id' => $this->id])
            ->all(), 'hotel_service_id', 'hotel_service_id');

    }
}

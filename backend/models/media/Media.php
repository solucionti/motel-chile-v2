<?php

namespace app\models\media;

use Yii;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property int $uid
 * @property string $entity
 * @property string $title
 * @property string $description
 * @property string $uri
 * @property string $type
 * @property string $size
 * @property int $weight
 */
class Media extends \yii\db\ActiveRecord
{
    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'safe'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['uid', 'weight'], 'integer'],
            [['entity', 'title', 'description', 'uri', 'type', 'size'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'uid'         => Yii::t('app', 'Uid'),
            'entity'      => Yii::t('app', 'Entity'),
            'title'       => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'uri'         => Yii::t('app', 'Uri'),
            'type'        => Yii::t('app', 'Type'),
            'size'        => Yii::t('app', 'Size'),
            'weight'      => Yii::t('app', 'Weight'),
        ];
    }


    public function createThumbail($image_path, $directory, $fileName, array $options)
    {
        if (!$options['width'] || !$options['height']) {
            return;
        }

        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }

        $thumb_path = $options['width'] . 'x' . $options['height'] . '_' . $fileName;
        Image::thumbnail($image_path, $options['width'], $options['height'])
            ->save($directory . $thumb_path, ['quality' => 100]);

        return [
            'thumb'      => $thumb_path,
            'public_url' => Yii::$app->params['URL_FRONT_PUBLIC_FILES_THUMBS'] . $thumb_path,
        ];
    }

    public function uploadImage()
    {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        $image = UploadedFile::getInstance($this, 'image');

        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }

        // store the source file name
        $this->filename = $image->name;
        $ext = $this->getExtension($image);

        // generate a unique file name
        $this->avatar = Yii::$app->security->generateRandomString() . ".{$ext}";

        // the uploaded image instance
        return $image;
    }

    /**
     * fetch stored image file name with complete path
     * @return string
     */
    public function getImageFile()
    {
        return isset($this->image) ? Yii::$app->params['uploadPath'] . $this->image : null;
    }


    /**
     * @param $image
     * @return mixed
     */
    public function getExtension($image)
    {


        $ext = (explode(".", $image->name));

        return end($ext);
    }

}

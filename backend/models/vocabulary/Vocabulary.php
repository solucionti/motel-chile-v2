<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vocabulary".
 *
 * @property int $id
 * @property string $title
 * @property string $machine_name
 * @property string $description
 *
 * @property Taxonomy[] $taxonomies
 */
class Vocabulary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vocabulary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'machine_name'], 'required'],
            [['description'], 'string'],
            [['title', 'machine_name'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['machine_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'machine_name' => Yii::t('app', 'Machine Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxonomies()
    {
        return $this->hasMany(Taxonomy::className(), ['vocabulary_id' => 'id'])->inverseOf('vocabulary');
    }
}

<?php

namespace app\models\vocabulary;

use Yii;

/**
 * This is the model class for table "taxonomy".
 *
 * @property int $id
 * @property int $vocabulary_id
 * @property int $parent
 * @property string $title
 * @property string $machine_name
 * @property string $description
 *
 * @property HotelHasService[] $hotelHasServices
 * @property HotelTaxonomyService[] $hotelTaxonomyServices
 * @property Taxonomy $parent0
 * @property Taxonomy[] $taxonomies
 * @property Vocabulary $vocabulary
 */
class Taxonomy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vocabulary_id', 'title', 'machine_name'], 'required'],
            [['vocabulary_id', 'parent'], 'integer'],
            [['description'], 'string'],
            [['title', 'machine_name'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['machine_name'], 'unique'],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => Taxonomy::className(), 'targetAttribute' => ['parent' => 'id']],
            [['vocabulary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vocabulary::className(), 'targetAttribute' => ['vocabulary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vocabulary_id' => Yii::t('app', 'Vocabulary ID'),
            'parent' => Yii::t('app', 'Parent'),
            'title' => Yii::t('app', 'Title'),
            'machine_name' => Yii::t('app', 'Machine Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelHasServices()
    {
        return $this->hasMany(HotelHasService::className(), ['hotel_service_id' => 'id'])->inverseOf('hotelService');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelTaxonomyServices()
    {
        return $this->hasMany(HotelTaxonomyService::className(), ['taxonomy_id' => 'id'])->inverseOf('taxonomy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(Taxonomy::className(), ['id' => 'parent'])->inverseOf('taxonomies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxonomies()
    {
        return $this->hasMany(Taxonomy::className(), ['parent' => 'id'])->inverseOf('parent0');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVocabulary()
    {
        return $this->hasOne(Vocabulary::className(), ['id' => 'vocabulary_id'])->inverseOf('taxonomies');
    }
}

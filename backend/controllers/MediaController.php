<?php

namespace backend\controllers;

use app\models\media\Media;
use Yii;

use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class MediaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['POST'],
                    'imageupload' => ['POST'],
                ],
            ],
        ];
    }

    public function actionImageupload($type, $id)
    {
        $model = new Media();


        $imageFile = UploadedFile::getInstance($model, 'image[' . $id . ']');


        $directory = Yii::getAlias('@frontend/web/uploads/');
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }
        //return Json::encode(['data' => $imageFile]);

        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            if ($imageFile->saveAs($filePath)) {
                $thumb = $model->createThumbail($filePath, $directory . '/thumbs/', $fileName, ['width' => 100, 'height' => '100']);

                // $path = Yii::$app->params['URL_FRONT_PUBLIC_FILES'] . $fileName;

                $model->uid = $id;
                $model->entity = $type;
                $model->uri = $fileName;
                $model->save();

                return Json::encode([
                    'files' => [
                        [
                            'name'         => $fileName,
                            'size'         => $imageFile->size,
                            'url'          => '',
                            'thumbnailUrl' => $thumb['public_url'],
                            'deleteUrl'    => 'image-delete?name=' . $fileName,
                            'deleteType'   => 'POST',
                        ],
                    ],
                ]);
            }
        }

        return Json::encode(['data' => 'Error desconocido']);


        return '';
    }

    public function actionImageDelete($name)
    {
        $directory = Yii::getAlias('@frontend/web/img/temp') . DIRECTORY_SEPARATOR . Yii::$app->session->id;
        if (is_file($directory . DIRECTORY_SEPARATOR . $name)) {
            unlink($directory . DIRECTORY_SEPARATOR . $name);
        }

        $files = FileHelper::findFiles($directory);
        $output = [];
        foreach ($files as $file) {
            $fileName = basename($file);
            $path = '/img/temp/' . Yii::$app->session->id . DIRECTORY_SEPARATOR . $fileName;
            $output['files'][] = [
                'name'         => $fileName,
                'size'         => filesize($file),
                'url'          => $path,
                'thumbnailUrl' => $path,
                'deleteUrl'    => 'image-delete?name=' . $fileName,
                'deleteType'   => 'POST',
            ];
        }

        return Json::encode($output);
    }

}

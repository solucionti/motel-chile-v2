<?php

namespace backend\controllers;


use app\models\media\Media;
use Yii;
use app\models\hotel\Hotel;
use app\models\hotel\HotelRoom;
use app\models\hotel\HotelSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;


/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class HotelroomController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionGallery($id)
    {
        $model = $this->findModel($id);

        return $this->renderAjax('gallery', [
            'model' => $model,
            'media'=>new Media()

        ]);
    }

    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HotelRoom::findOne($id)) !== null) {


            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

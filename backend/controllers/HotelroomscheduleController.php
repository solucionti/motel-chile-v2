<?php

namespace backend\controllers;


use app\models\hotel\HotelRoom;
use app\models\Model;
use Yii;
use app\models\hotel\HotelRoomSchedule;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;


/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class HotelroomscheduleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


//Yii::$app->response->format = Response::FORMAT_JSON;
//
//return [
//'success' => true,
//'data'    => [
//'refresh' => $schedule
//]
//];
//}

    /**
     * Creates a new Hotel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $modelCustomer = HotelRoom::findOne($id);


        $modelsAddress = $modelCustomer->hotelRoomSchedules;

        if ($modelCustomer->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsAddress, 'id', 'id');


            $modelsAddress = Model::createMultiple(HotelRoomSchedule::classname(), $modelsAddress);

            Model::loadMultiple($modelsAddress, Yii::$app->request->post());


            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsAddress, 'id', 'id')));

            // validate all models
            $valid = $modelCustomer->validate();

            foreach ($modelsAddress as $modelsAddres) {
                $modelsAddres->hotel_room_id = $modelCustomer->id;
            }

            $valid = Model::validateMultiple($modelsAddress) && $valid;


            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelCustomer->save(false)) {
                        if (!empty($deletedIDs)) {
                            HotelRoomSchedule::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsAddress as $modelAddress) {
                            if (!($flag = $modelAddress->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();


                        Yii::$app->response->format = Response::FORMAT_JSON;

                        return [
                            'success' => true,
                            'data'    => [
                                'refresh' => 'bien'
                            ]
                        ];
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'success' => true,
            'data'    => [
                'refresh' => 'Algo mal'
            ]
        ];
    }


    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HotelRoomSchedule::findOne($id)) !== null) {


            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

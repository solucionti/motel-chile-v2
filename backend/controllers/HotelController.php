<?php

namespace backend\controllers;

use app\models\hotel\HotelHasService;
use app\models\hotel\HotelTaxonomyService;
use app\models\media\Media;
use Yii;
use app\models\hotel\Hotel;
use app\models\hotel\HotelRoom;
use app\models\hotel\HotelSearch;
use app\models\Model;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;


/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class HotelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hotel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HotelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hotel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hotel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Hotel;
        $model_room = new HotelRoom();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model'      => $model,
                'model_room' => $model_room,
            ]);
        }
    }


    public function actionCreateroom()
    {
        $model = new HotelRoom;


        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->save()) {
                if (Yii::$app->request->isAjax) {

                    $model_hotel = $this->findModel($model->hotel_id);

                    $rooms_table = $this->renderAjax('partials/_rooms_table', [
                        'model' => $model_hotel,
                    ]);

                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return [
                        'success' => true,
                        'data'    => [
                            'refresh' => $rooms_table
                        ]
                    ];
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return [
                        'error'  => true,
                        'errors' => $model->getErrors()
                    ];
                }
            }
        }
    }

    /**
     * Updates an existing Hotel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_room = new HotelRoom();
        $media = new Media();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'      => $model,
                'model_room' => $model_room,
                'media'      => $media,

            ]);
        }
    }

    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionHotelroomdelete($id)
    {
        $model = $this->findModelHotelRoom($id);

        $hotel_id=$model->hotel_id;

        $model->delete();

        return $this->redirect(['hotel/update',
            'id' => $hotel_id
        ]);

    }

    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelHotelRoom($id)
    {
        if (($model = HotelRoom::findOne($id)) !== null) {


            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hotel::findOne($id)) !== null) {

            $model->services = $model->getServicesAsArray();

            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

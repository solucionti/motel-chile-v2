<?php

namespace backend\controllers;


use app\models\hotel\HotelRoom;
use app\models\Model;
use Yii;
use app\models\hotel\HotelRoomSchedule;
use app\models\hotel\HotelRoomHasService;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;


/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class HotelroomserviceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Creates a new Hotel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRoomservice($id)
    {
        if (!$hotelRoom = HotelRoom::findOne($id)) {
            return;
        }


        if ($room_services = Yii::$app->request->post('HotelRoomHasService')) {

            $hotelRoom->deleteAllService();

            if (!$room_services['services']) {
                return;
            }

            foreach ($room_services['services'] as $roomService) {


                $new_hotel_service = new HotelRoomHasService();
                $new_hotel_service->hotel_room_id = $id;
                $new_hotel_service->service_id = $roomService;

                if ($new_hotel_service->validate()) {
                    $new_hotel_service->save();
                }
            }


            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'success' => true,
                'data'    => [
                    'status' => 'ok'
                ]
            ];
        }

        return [
            'success' => true,
            'data'    => [
                'status' => 'error'
            ]
        ];

        return;
    }


    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HotelRoomHasService::findOne($id)) !== null) {

            $model->services = $model->getServicesAsArray();


            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

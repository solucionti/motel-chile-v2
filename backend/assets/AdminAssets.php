<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AdminAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/admin';
    public $css = [
        '/themes/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css',
        '/themes/admin/assets/vendors/base/vendors.bundle.css',
        '/themes/admin/assets/demo/default/base/style.bundle.css',
        '/themes/admin/assets/demo/default/media/img/logo/favicon.ico',
        '/themes/admin/assets/css/custom.css',
    ];
    public $js = [
        '/themes/admin/assets/vendors/base/vendors.bundle.js',
        '/themes/admin/assets/demo/default/base/scripts.bundle.js',
        '/themes/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js',
        '/themes/admin/assets/app/js/dashboard.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',


    ];

    public $jsOptions = array(
      'position' => \yii\web\View::POS_HEAD
    );

}

<?php

namespace common\components\helpers;

use app\models\vocabulary\Taxonomy;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: camilo
 * Date: 28-01-18
 * Time: 12:14
 */
class HelperClass
{

    public static function getDaysOfWeek()
    {
        return
            [
                0 => 'Lunes',
                1 => 'Martes',
                2 => 'MIercoles',
                3 => 'Jueves',
                4 => 'Viernes',
                5 => 'Sabado',
                6 => 'Domingo',

            ];
    }

    public static function getHoursOfDay()
    {



        for($i=1;$i<=24;$i++){
            $_hours[$i]=$i;
        }

        return $_hours;

    }


}
<?php

namespace common\components\taxonomy;

use app\models\vocabulary\Taxonomy;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: camilo
 * Date: 28-01-18
 * Time: 12:14
 */
class TaxonomyClassHelper
{

    public static function listServices()
    {

        $services = ArrayHelper::map(Taxonomy::find()->asArray()->select('id, title')->where(['vocabulary_id' => 1])->orderBy('title')->all(), 'id', 'title');

        //$services = Taxonomy::find()->where(['vocabulary_id' => 1])->all();

        return $services;
    }

}